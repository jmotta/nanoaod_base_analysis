Luminosity json filter
======================

.. currentmodule:: Base.Modules.lumiFilter

.. contents::

``lumiFilterRDF``
-----------------

.. autoclass:: lumiFilterRDF
   :members:

