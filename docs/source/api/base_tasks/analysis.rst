cmt.base_tasks.analysis
=======================

.. automodule:: cmt.base_tasks.analysis

.. contents::

Class ``FitBase``
-----------------

.. autoclass:: FitBase
   :members:

Class ``CombineBase``
---------------------

.. autoclass:: CombineBase
   :members:

Class ``CombineCategoriesTask``
-------------------------------

.. autoclass:: CombineCategoriesTask
   :members:

Class ``CreateDatacards``
-------------------------

.. autoclass:: CreateDatacards
   :members:

Class ``Fit``
-------------

.. autoclass:: Fit
   :members:

Class ``InspectFitSyst``
------------------------

.. autoclass:: InspectFitSyst
   :members:

Class ``CombineDatacards``
--------------------------

.. autoclass:: CombineDatacards
   :members:

Class ``CreateWorkspace``
-------------------------

.. autoclass:: CreateWorkspace
   :members:


Class ``RunCombine``
--------------------

.. autoclass:: RunCombine
   :members:


Class ``PullsAndImpacts``
-------------------------

.. autoclass:: PullsAndImpacts
   :members:

Class ``MergePullsAndImpacts``
------------------------------

.. autoclass:: MergePullsAndImpacts
   :members:

Class ``PlotPullsAndImpacts``
-----------------------------

.. autoclass:: PlotPullsAndImpacts
   :members:

