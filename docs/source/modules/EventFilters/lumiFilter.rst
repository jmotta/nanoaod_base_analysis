Luminosity json filter
======================

.. currentmodule:: Event.Filters.lumiFilter

.. contents::

``lumiFilterRDF``
-----------------

.. autoclass:: lumiFilterRDF
   :members:

